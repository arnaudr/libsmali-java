# Debian baksmali completion                             -*- shell-script -*-

_baksmali()
{
    local cur prev words cword
    _init_completion || return

    # see if the user selected a command already
    local COMMANDS=(
        "deodex"
        "disassemble"
        "dump"
        "help"
        "list"
    )

    local command i
    for (( i=0; i < ${#words[@]}-1; i++ )); do
        if [[ ${COMMANDS[@]} =~ ${words[i]} ]]; then
            command=${words[i]}
            break
        fi
    done

    # Complete a --option<SPACE><TAB>
    case $prev in
	-d|--classpath-dir|--cpd|--dir)
            _filedir -d
            return 0
            ;;
	--inline-table|--inline|--it)
            _filedir
            return 0
            ;;
        -o|--output)
            _filedir
            return 0
            ;;
    esac

    # supported options per command
    if [[ "$cur" == -* ]]; then
        case $command in
            deodex|de|x)
                COMPREPLY=($(compgen -W '
                    -a --api
                    --accessor-comments
                    --allow-odex-opcodes
                    -b --bootclasspath
                    -c --classpath
                    --check-package-private-access --package-private
                    --classes
                    --code-offsets --offsets
                    -d --classpath-dir --dir
                    --debug-info
                    -h --help
                    --implicit-references
                    --inline-table
                    -j --jobs
                    -l --use-locals
                    --normalize-virtual-methods
                    -o --output
                    --parameter-registers
                    -r --register-info
                    --resolve-resources
                    --sequential-labels
                '))
                return 
                ;;
            disassemble|dis|d)
                COMPREPLY=($(compgen -W '
                    -a --api
                    --accessor-comments
                    --allow-odex-opcodes
                    -b --bootclasspath
                    -c --classpath
                    --classes
                    --code-offsets --offsets
                    -d --classpath-dir --dir
                    --debug-info
                    -h --help
                    --implicit-references
                    -j --jobs
                    -l --use-locals
                    --normalize-virtual-methods
                    -o --output
                    --parameter-registers
                    -r --register-info
                    --resolve-resources
                    --sequential-labels
                '))
                return 0
                ;;
            dump|du)
                COMPREPLY=($(compgen -W '-a --api -h --help'))
                return 0
                ;;
            list|l)
                COMPREPLY=($(compgen -W '-h --help'))
                return 0
                ;;
            help)
                return 0
                ;;
        esac
    fi

    # specific command arguments
    if [[ -n $command ]]; then
        case $command in
            deodex|de|x)
                _filedir '@(apk|dex|oat|odex)'
                return 0
                ;;
            disassemble|dis|d)
                _filedir '@(apk|dex|oat|odex)'
                return 0
                ;;
            dump|du)
                _filedir '@(apk|dex|oat|odex)'
                return 0
                ;;
            list|l)
                COMPREPLY=($(compgen -W '
                      classes
                      dependencies
                      dex
                      fieldoffsets
                      fields
                      help
                      methods
                      strings
                      types
                      vtables
                '))
                return 0
                ;;
        esac
    fi

    # no command yet, show what commands we have
    if [ "$command" = "" ]; then
        COMPREPLY=( $( compgen -W '${COMMANDS[@]}' -- "$cur" ) )
    fi

    return 0
} &&
complete -F _baksmali baksmali

# ex: ts=4 sw=4 et filetype=sh
