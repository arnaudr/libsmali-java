libsmali-java (2.5.2.git2771eae-3) unstable; urgency=medium

  * switch to salsa-ci.yml
  * work around missing dexdump in many arches

 -- Hans-Christoph Steiner <hans@eds.org>  Tue, 14 Feb 2023 15:58:05 +0100

libsmali-java (2.5.2.git2771eae-2) unstable; urgency=medium

  * apkpgtest Depends: default-jre-headless (Closes: #1030965)

 -- Hans-Christoph Steiner <hans@eds.org>  Mon, 13 Feb 2023 12:42:17 +0100

libsmali-java (2.5.2.git2771eae-1) unstable; urgency=medium

  [ Andrej Shadura ]
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository, Repository-Browse.

  [ Raman Sarda ]
  * Added Root-Requires-Root: No

  [ Hans-Christoph Steiner ]
  * gitlab-ci: don't run on tags
  * New upstream version 2.5.2.git2771eae
  * add command line versions, test in autopkgtest

 -- Hans-Christoph Steiner <hans@eds.org>  Tue, 07 Feb 2023 21:25:53 +0100

libsmali-java (2.4.0-1) unstable; urgency=medium

  * New upstream version 2.4.0.
  * Refresh remove-troublesome-jgit-tricks.patch
  * Declare compliance with Debian Policy 4.5.0.

 -- Markus Koschany <apo@debian.org>  Sun, 29 Mar 2020 22:13:58 +0200

libsmali-java (2.2.7-1) unstable; urgency=medium

  [ Hans-Christoph Steiner ]
  * gitlab-ci: update to generate aptly repo

  [ Markus Koschany ]
  * New upstream version 2.2.7.
  * Switch to debhelper-compat = 12.
  * Declare compliance with Debian Policy 4.4.0.

 -- Markus Koschany <apo@debian.org>  Mon, 22 Jul 2019 02:53:27 +0200

libsmali-java (2.2.6-1) unstable; urgency=medium

  * New upstream version

 -- Hans-Christoph Steiner <hans@eds.org>  Sat, 26 Jan 2019 21:25:48 +0000

libsmali-java (2.2.5-1) unstable; urgency=medium

  * New upstream version

 -- Hans-Christoph Steiner <hans@eds.org>  Mon, 21 Jan 2019 12:13:28 +0100

libsmali-java (2.2.3-1) unstable; urgency=medium

  [ 殷啟聰 | Kai-Chung Yan ]
  * d/maven.rules: Remove jcommander (Closes: #894676)
  * d/control: Build-Depends on default-jre => default-jre-headless

  [ Markus Koschany ]
  * New upstream version 2.2.3.
  * Update VCS fields.
  * Declare compliance with Debian Policy 4.1.4.

 -- Markus Koschany <apo@debian.org>  Thu, 12 Apr 2018 19:28:24 +0200

libsmali-java (2.2.2-1) unstable; urgency=medium

  * New upstream version 2.2.2.
  * Switch to compat level 11.
  * Declare compliance with Debian Policy 4.1.3.
  * Install the NOTICE file with debian/docs.

 -- Markus Koschany <apo@debian.org>  Thu, 04 Jan 2018 22:37:28 +0100

libsmali-java (2.2.1-2) unstable; urgency=medium

  * Upload to unstable.
  * Tighten smali dependencies in pom files.

 -- Markus Koschany <apo@debian.org>  Mon, 19 Jun 2017 00:13:00 +0200

libsmali-java (2.2.1-1) experimental; urgency=medium

  [ Hans-Christoph Steiner ]
  * add myself to Uploaders:
  * update Build-Depends: for new upstream release
  * use proguard-cli to avoid pulling in 140 GUI deps

  [ Markus Koschany ]
  * New upstream version 2.2.1.
  * Switch to compat level 10.
  * Update pom versions for new release.
  * Update copyright years.
  * Refresh revert-switch-to-jprante-s-jflex-plugin.patch.
  * Fix maven rule for libjcommander-java and ensure that we got no FTBFS.

 -- Markus Koschany <apo@debian.org>  Sun, 18 Jun 2017 00:06:00 +0200

libsmali-java (2.1.3-1) unstable; urgency=medium

  * Imported Upstream version 2.1.3.
  * debian/watch: Use version=4.
  * Use Android Tools Maintainers as name.
  * Add missing Apache-2.0 licensed files to debian/copyright.
  * Update short description.
  * Add build.patch and always build Java 6 class files. (Closes: #832568)
  * Remove lintian-override.
  * Tighten build-dependency on jflex and libgradle-jflex-plugin-java.
  * libsmali-java.poms: Use wildcards for version.
  * Update debian/copyright for new release.
  * Update pom files for new release.

 -- Markus Koschany <apo@debian.org>  Wed, 27 Jul 2016 15:28:18 +0200

libsmali-java (2.1.2-1) unstable; urgency=medium

  * Initial release (Closes: #808397)

 -- Markus Koschany <apo@debian.org>  Sat, 23 Apr 2016 08:13:42 +0200
